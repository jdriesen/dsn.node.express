# What is this all about ?

- Super Basic Node Express Server
- apiTester Playground (Using the vsCode REST Client plugin )
  This Plugin is a handy replacement for PostMan...(you don't have to "leave" vsCode to perfom API Tests)

# Installation

## Don't forget to install the REST Client Plugin in vsCode !!!

run `npm install`  
run `npm start` ((will start a basic Node Express Server on port 3000))

# Let's have some fun ...

Click on an API request in apiTester.http (results are shown in the right screen of vsCode)
