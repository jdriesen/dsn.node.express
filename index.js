// Using ES6 imports
// This is possible due to the type=module setting in package.json
import Express, { urlencoded } from "express";

import Products from "./products.js";

console.log("start");

const PORT = 3000;
const app = Express();
app.use(Express.json());
app.use(Express.urlencoded({ extended: true }));

// Example of Middleware
const mid = (req, res, next) => {
  console.log("Output of Middleware Function");
  console.log("req.params", req.params);
  console.log("req.query", req.query);
  next();
};

// Get ALL Products
app.get("/products", (req, res) => {
  //res.send("Hello World");
  res.json(Products);
});

// Get ONE Product by ID
// Example of using Middleware
app.get("/products/:id", mid, (req, res) => {
  res.json(
    Products.find((product) => {
      return +req.params.id === product.id;
    })
  );
});

// Get ONE Product by ID
app.post("/products/add", (req, res) => {
  console.log(req.body.id);
  res.sendStatus(200);
});

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
